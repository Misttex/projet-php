<?php
// Basculer à TRUE pour activer les affichages de debug, les var_dump ou les dump_var
define('DUMP', true);
define('DUMP_EXCEPTIONS', false);
// L'url de votre site, sera utile dans les pages en cas de déplacement du site...
define('URL_BASE', 'http://MonDomaine.com/');
// Les informations de connexion à la BDD
$infoBdd = array(
    'interface' => 'pdo', // "pdo" ou "mysqli"
    'type' => 'pgsql', // mysql ou pgsql
    'host' => 'localhost', // Votre serveur de bdd
    'port' => 5432, // Par défaut: 5432 pour postgreSQL, 3306 pour MySQL
    'charset' => 'UTF8', // charset de la bdd
    'dbname' => 'competences', // Le nom de la bdd
    'user' => 'admin', // l'utilisateur de la bdd
    'pass' => '', // le password de l'utilisateur de la bdd
);
// Inclusion de la configuration globale
require_once 'globalConfig.php';
try {
// Lancement des sessions, si ce n'est pas déjà fait.
// Le faire après l'inclusion de 'globalConfig.php' permet d'avoir l'autoload actif
// et de pouvoir désérializer des objets depuis les sessions.
    if (session_status() === PHP_SESSION_NONE) {
        session_start();
    }
} catch (Throwable $ex) {
    var_dump($ex);
}
