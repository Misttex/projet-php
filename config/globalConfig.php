<?php
define('BASE_DIR',dirname(__FILE__));
define('SRC_DIR',
    BASE_DIR . DIRECTORY_SEPARATOR.
    'src' . DIRECTORY_SEPARATOR);
define('CONFIG_DIR',BASE_DIR.DIRECTORY_SEPARATOR.'config'.DIRECTORY_SEPARATOR);
define('PUBLIC_DIR',BASE_DIR.DIRECTORY_SEPARATOR.'public'.DIRECTORY_SEPARATOR);
define('VENDOR_DIR', BASE_DIR.DIRECTORY_SEPARATOR.'vendor'.DIRECTORY_SEPARATOR);
define('PHALN_DIR', BASE_DIR.DIRECTORY_SEPARATOR.'vendor'.DIRECTORY_SEPARATOR.'vendor'.DIRECTORY_SEPARATOR.'phaln'.DIRECTORY_SEPARATOR);
define('LOG_DIR', BASE_DIR.DIRECTORY_SEPARATOR.'logs'.DIRECTORY_SEPARATOR);
// Si la librairie Phaln est utilisée, il faut la configurer avec
// les informations de connexion à la base de données.
// Ces informations se trouve dans le fichier /config/appConfig.php
    if (file_exists(PHALN_DIR . DIRECTORY_SEPARATOR . 'phaln' . DIRECTORY_SEPARATOR .
    'BDD.php') && class_exists('Phaln\BDD') && isset($infoBdd)) {
    Phaln\BDD::$infoBdd = $infoBdd;
    }
// Active ou pas l'affichage de debug et les dump_var
    if (DUMP) { // Affiche les dumps et les throwables
    error_reporting(E_ALL);
    ini_set('display_errors', 'On');
    } else {
        if (DUMP_EXCEPTIONS) { // Affiche les throwables mais pas les dumps
            error_reporting(E_ALL);
            ini_set('display_errors', 'On');
        } else { // Affiche rien --> fichiers de log
            ini_set('display_errors', 'Off');
            ini_set('log_errors', 'On');
            ini_set('error_log', LOG_DIR . 'error_log.txt');
        }
    }
// Utilise le fichier de configuration de la librairie PhAln
// Ce fichier contient aussi la définition de la fonction dump_var() assez utile...
    $phalnConfigFile = PHALN_DIR . DIRECTORY_SEPARATOR . 'phaln' . DIRECTORY_SEPARATOR .
        'config' . DIRECTORY_SEPARATOR . 'phalnConfig.php';
    if (file_exists($phalnConfigFile)) {
        require_once($phalnConfigFile);
    }
