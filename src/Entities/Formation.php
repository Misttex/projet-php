<?php

namespace App\Entities;
use Phaln\AbstractEntity;

class Formation extends  AbstractEntity
{
    protected Int $id;
    protected String $intitule;
    protected Int $niveau;
}